<main class='content floorplan'>
	<img src="<?= FRONT_ASSETS ?>img/map.png">

	<div class='info'>
        <p id='name'></p>
    </div>

	<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="1080" height="1920" viewBox="0 0 1080 1920">
		  <defs>
		    <style>
		      .cls-1 {
		        fill: #fff;
		        opacity: 0;
		      }
		    </style>
		  </defs>
		  <rect data-name='Romilly Cidre' class="booth cls-1" x="448" y="292" width="17" height="36"/>
		  <rect data-name='Carried Away Chefs' class="booth cls-1" x="448" y="258" width="17" height="34"/>
		  <rect data-name='Angel of Harlem' class="booth cls-1" x="448" y="223" width="17" height="35"/>
		  <rect data-name='Maison Marcel' class="booth cls-1" x="444" y="117" width="17" height="34"/>
		  <rect data-name='Jeepeny Filipino Gastropub' class="booth cls-1" x="443" y="74" width="17" height="34"/>
		  <rect data-name='Tommy Bahama Restaurant - Bar - Store' class="booth cls-1" x="478.5" y="37.5" width="17" height="34" transform="translate(541.5 -432.5) rotate(90)"/>
		  <rect data-name='Ortzi NYC' class="booth cls-1" x="514.5" y="37.5" width="17" height="34" transform="translate(577.5 -468.5) rotate(90)"/>
		  <rect data-name='Finger Lakes Distilling' class="booth cls-1" x="552.5" y="37.5" width="17" height="34" transform="translate(615.5 -506.5) rotate(90)"/>
		  <rect data-name='Hunt & Fish Club NYC' class="booth cls-1" x="590.5" y="37.5" width="17" height="34" transform="translate(653.5 -544.5) rotate(90)"/>
		  <rect data-name="L'Angolo Tribeca" class="booth cls-1" x="627.5" y="37.5" width="17" height="34" transform="translate(690.5 -581.5) rotate(90)"/>
		  <rect data-name='Motel Morris' class="booth cls-1" x="665.5" y="37.5" width="17" height="34" transform="translate(728.5 -619.5) rotate(90)"/>
		  <rect data-name="L'Amico/Second" class="booth cls-1" x="702.5" y="37.5" width="17" height="34" transform="translate(765.5 -656.5) rotate(90)"/>
		  <rect data-name="Brotherhood America's Oldest Winery" class="booth cls-1" x="740.5" y="37.5" width="17" height="34" transform="translate(803.5 -694.5) rotate(90)"/>
		  <rect data-name='Nick's Lobster house class="booth cls-1" x="776.5" y="37.5" width="17" height="34" transform="translate(839.5 -730.5) rotate(90)"/>
		  <rect data-name="Frito con cito" class="booth cls-1" x="813.5" y="37.5" width="17" height="34" transform="translate(876.5 -767.5) rotate(90)"/>
		  <rect data-name="Good Enough To EAt" class="booth cls-1" x="864" y="73" width="17" height="34"/>
		  <rect data-name="Health-ADE" class="booth cls-1" x="863" y="124" width="17" height="34"/>
		  <rect data-name="Jora Restaurant and Bar" class="booth cls-1" x="555.5" y="147.5" width="17" height="34" transform="translate(728.5 -399.5) rotate(90)"/>
		  <rect data-name="Pisco100 perfectly peruvian" class="booth cls-1" x="589.5" y="147.5" width="17" height="34" transform="translate(762.5 -433.5) rotate(90)"/>
		  <rect data-name="sen sakana" class="booth cls-1" x="623.5" y="147.5" width="17" height="34" transform="translate(796.5 -467.5) rotate(90)"/>
		  <rect data-name="cuque&#xf1;a beer" class="booth cls-1" x="665.5" y="147.5" width="17" height="34" transform="translate(838.5 -509.5) rotate(90)"/>
		  <rect data-name="Panca" class="booth cls-1" x="704.5" y="147.5" width="17" height="34" transform="translate(877.5 -548.5) rotate(90)"/>
		  <rect data-name="Surfish Bistro" class="booth cls-1" x="743.5" y="147.5" width="17" height="34" transform="translate(916.5 -587.5) rotate(90)"/>
		  <rect data-name="Baby Brasa" class="booth cls-1" x="781.5" y="147.5" width="17" height="34" transform="translate(954.5 -625.5) rotate(90)"/>
		  <rect data-name="Todd English" class="booth cls-1" x="555.5" y="203.5" width="17" height="34" transform="translate(784.5 -343.5) rotate(90)"/>
		  <rect data-name="Nobu Fifty Seven" class="booth cls-1" x="590.5" y="203.5" width="17" height="34" transform="translate(819.5 -378.5) rotate(90)"/>
		  <rect data-name="The long Drink Company" class="booth cls-1" x="624.5" y="203.5" width="17" height="34" transform="translate(853.5 -412.5) rotate(90)"/>
		  <rect data-name="PAtina Restaurant group at rockefeller center" class="booth cls-1" x="666.5" y="203.5" width="17" height="34" transform="translate(895.5 -454.5) rotate(90)"/>
		  <rect data-name="rock center cafe" class="booth cls-1" x="704.5" y="203.5" width="17" height="34" transform="translate(933.5 -492.5) rotate(90)"/>
		  <rect data-name="Cucina & co." class="booth cls-1" x="743.5" y="203.5" width="17" height="34" transform="translate(972.5 -531.5) rotate(90)"/>
		  <rect data-name="PAtina Restaurant group - rock center cafe" class="booth cls-1" x="781.5" y="203.5" width="17" height="34" transform="translate(1010.5 -569.5) rotate(90)"/>
		  <rect data-name="Sweet Rae's vookies" class="booth cls-1" x="882" y="336" width="17" height="34"/>
		  <rect data-name="jessie's nutty cups" class="booth cls-1" x="903" y="336" width="17" height="34"/>
		  <rect data-name="Frutapop" class="booth cls-1" x="882" y="372" width="17" height="34"/>
		  <rect data-name="Talty Bar" class="booth cls-1" x="903" y="372" width="17" height="34"/>
		  <rect data-name="Drunken Fruit" class="booth cls-1" x="883" y="511" width="17" height="34"/>
		  <rect data-name="The Vegantry" class="booth cls-1" x="906" y="511" width="17" height="34"/>
		  <rect data-name="Vita Coco" class="booth cls-1" x="883" y="546" width="17" height="34"/>
		  <rect data-name="Allie's GF Goodies" class="booth cls-1" x="906" y="546" width="17" height="34"/>
		  <rect data-name="Buddy's Allergen Free" class="booth cls-1" x="947" y="219" width="17" height="34"/>
		  <rect data-name="Melt Bakery" class="booth cls-1" x="947" y="255" width="17" height="34"/>
		  <rect data-name="Dallis Bros Coffee" class="booth cls-1" x="976" y="311" width="17" height="34"/>
		  <rect data-name="Meska Sweets" class="booth cls-1" x="976" y="345" width="17" height="34"/>
		  <rect data-name="Loi Estiatorio" class="booth cls-1" x="976" y="379" width="17" height="34"/>
		  <rect data-name="Mochidoki" class="booth cls-1" x="976" y="411" width="17" height="34"/>
		  <rect data-name="Serendipity 3" class="booth cls-1" x="961" y="462" width="17" height="34"/>
		  <rect data-name="sarabeth's" class="booth cls-1" x="962" y="496" width="17" height="34"/>
		  <rect data-name="a little brittle heaven" class="booth cls-1" x="983" y="557" width="17" height="34"/>
		  <rect data-name="javamelts" class="booth cls-1" x="983" y="594" width="17" height="34"/>
		  <rect data-name="insomnia cookies" class="booth cls-1" x="983" y="631" width="17" height="34"/>
		  <rect data-name="magnolia bakery" class="booth cls-1" x="983" y="668" width="17" height="34"/>
		  <rect data-name="bonsai kakigori" class="booth cls-1" x="983" y="706" width="17" height="34"/>
		  <rect data-name="wb law coffee co." class="booth cls-1" x="951.5" y="744.5" width="17" height="34" transform="translate(1721.5 -198.5) rotate(90)"/>
		  <rect data-name="treat house" class="booth cls-1" x="910.5" y="744.5" width="17" height="34" transform="translate(1680.5 -157.5) rotate(90)"/>
		  <rect data-name="porter house bar and grill" class="booth cls-1" x="569.5" y="771.5" width="17" height="34" transform="translate(1366.5 210.5) rotate(90)"/>
		  <rect data-name="lavo" class="booth cls-1" x="612.5" y="771.5" width="17" height="34" transform="translate(1409.5 167.5) rotate(90)"/>
		  <rect data-name="tao" class="booth cls-1" x="663.5" y="771.5" width="17" height="34" transform="translate(1460.5 116.5) rotate(90)"/>
		  <rect data-name="otto's tacos" class="booth cls-1" x="697.5" y="771.5" width="17" height="34" transform="translate(1494.5 82.5) rotate(90)"/>
		  <rect data-name="barbounia" class="booth cls-1" x="731.5" y="771.5" width="17" height="34" transform="translate(1528.5 48.5) rotate(90)"/>
		  <rect data-name="parkway bakery & tavern" class="booth cls-1" x="777.5" y="771.5" width="17" height="34" transform="translate(1574.5 2.5) rotate(90)"/>
		  <rect data-name="xyst" class="booth cls-1" x="814.5" y="771.5" width="17" height="34" transform="translate(1611.5 -34.5) rotate(90)"/>
		  <rect data-name="american freedom disillery" class="booth cls-1" x="569.5" y="824.5" width="17" height="34" transform="translate(1419.5 263.5) rotate(90)"/>
		  <rect data-name="morgan's brooklyn barbecue" class="booth cls-1" x="613.5" y="824.5" width="17" height="34" transform="translate(1463.5 219.5) rotate(90)"/>
		  <rect data-name="kaiso coconut water cocktail" class="booth cls-1" x="663.5" y="823.5" width="17" height="34" transform="translate(1512.5 168.5) rotate(90)"/>
		  <rect data-name="toloache" class="booth cls-1" x="697.5" y="823.5" width="17" height="34" transform="translate(1546.5 134.5) rotate(90)"/>
		  <rect data-name="the sea fire grill" class="booth cls-1" x="731.5" y="824.5" width="17" height="34" transform="translate(1581.5 101.5) rotate(90)"/>
		  <rect data-name="benjamin prime" class="booth cls-1" x="777.5" y="824.5" width="17" height="34" transform="translate(1627.5 55.5) rotate(90)"/>
		  <rect data-name="port morris Distillery" class="booth cls-1" x="814.5" y="824.5" width="17" height="34" transform="translate(1664.5 18.5) rotate(90)"/>
		  <rect data-name="tito's" class="booth cls-1" x="314.5" y="711.5" width="17" height="34" transform="translate(1051.5 405.5) rotate(90)"/>
		  <rect data-name="enjoy european quality" class="booth cls-1" x="381.5" y="771.5" width="17" height="34" transform="translate(1178.5 398.5) rotate(90)"/>
		  <rect data-name="wine spectator" class="booth cls-1" x="420.5" y="771.5" width="17" height="34" transform="translate(1217.5 359.5) rotate(90)"/>
		  <rect data-name="the smith" class="booth cls-1" x="450" y="811" width="17" height="36"/>
		  <rect data-name="boulud sud" class="booth cls-1" x="450" y="847" width="17" height="36"/>
		  <rect data-name="patrizia's of 2nd ave" class="booth cls-1" x="497.5" y="890.5" width="17" height="34" transform="translate(1413.5 401.5) rotate(90)"/>
		  <rect data-name="foragers table at foragers market" class="booth cls-1" x="537.5" y="889.5" width="17" height="34" transform="translate(1452.5 360.5) rotate(90)"/>
		  <rect data-name="ben & jacks steakhouse" class="booth cls-1" x="576.5" y="889.5" width="17" height="34" transform="translate(1491.5 321.5) rotate(90)"/>
		  <rect data-name="the kati roll company" class="booth cls-1" x="616.5" y="889.5" width="17" height="34" transform="translate(1531.5 281.5) rotate(90)"/>
		  <rect data-name="delicatessen" class="booth cls-1" x="655.5" y="890.5" width="17" height="34" transform="translate(1571.5 243.5) rotate(90)"/>
		  <rect data-name="cutwater spirits" class="booth cls-1" x="694.5" y="890.5" width="17" height="34" transform="translate(1610.5 204.5) rotate(90)"/>
		  <rect data-name="jing fong restaurant" class="booth cls-1" x="734.5" y="890.5" width="17" height="34" transform="translate(1650.5 164.5) rotate(90)"/>
		  <rect data-name="mr crabby's craft kitchen + bar" class="booth cls-1" x="774.5" y="890.5" width="17" height="34" transform="translate(1690.5 124.5) rotate(90)"/>
		  <rect data-name="caledonia spirits" class="booth cls-1" x="816.5" y="890.5" width="17" height="34" transform="translate(1732.5 82.5) rotate(90)"/>
		  <rect data-name="the algonquin hotel" class="booth cls-1" x="854.5" y="890.5" width="17" height="34" transform="translate(1770.5 44.5) rotate(90)"/>
		  <rect data-name="sweetcatch poke" class="booth cls-1" x="882" y="856" width="17" height="34"/>
		  <rect data-name="calle ocho" class="booth cls-1" x="882" y="818" width="17" height="34"/>
		  <rect data-name="ladresse" class="booth cls-1" x="882" y="777" width="17" height="34"/>
		  <rect data-name="murray river salt" class="booth cls-1" x="430.5" y="620.5" width="17" height="34" transform="translate(1076.5 198.5) rotate(90)"/>
		  <rect data-name="butterfly bakeshop" class="booth cls-1" x="403" y="595" width="17" height="36"/>
		  <rect data-name="stk steakhouse" class="booth cls-1" x="430.5" y="574.5" width="17" height="34" transform="translate(1030.5 152.5) rotate(90)"/>
		  <rect data-name="clarkson avenue crumb cake company" class="booth cls-1" x="456" y="595" width="17" height="36"/>
		</svg>
</main>

<script type="text/javascript">
	$('.booth').click(function(){
        $('.info').hide();
    });

    $('.booth').click(function(){
        $('.info').removeClass('right');
        var name = $(this).attr('data-name');
        var x = $(this)[0].getBoundingClientRect().x;
		var y = $(this)[0].getBoundingClientRect().y;
		var h = $(this)[0].getBoundingClientRect().height;
        var w = $(this)[0].getBoundingClientRect().width;


        if ( name ) {
            $('.info').fadeIn(500);
            $('.info #name').html(name);
            if ( x > 680 ) {
                x += w;
                $('.info').addClass('right');
            }
            $('.info').css({"top": y + h, "left":  x});
        }else {
            $('info').fadeOut(500);
        }
    });

    $('svg').click(function(e){
    	if ( $(e.target).attr('class') ) {
	        if ( !$(e.target).attr('class').includes('booth')) {
	            if ( $(e.target).parents('.booth').length === 0 ) {
	                $('.info').fadeOut(500);
	            }
	        }
    	}else {
    		$('.info').fadeOut(500);
    	}
    });
</script>