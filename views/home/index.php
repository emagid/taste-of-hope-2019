
<style type="text/css">
	.home_btn {
		display: none !important;
	}
</style>

<main class='content'>
	<img src="<?= FRONT_ASSETS ?>img/home.jpg">
	<div class='home_btns'>
		<a href='/home/photobooth' class='div_photobooth'></a>
		<a href='/home/honorees' class='div_honoree'></a>
		<a href='/home/sponsors' class='div_sponsors'></a>
		<a href='/home/donate' class='div_donate'></a>
		<a href='/home/floorplan' class='div_floorplan'></a>
	</div>
</main>
