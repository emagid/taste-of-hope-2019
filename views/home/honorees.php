<main class='content'>
	<div class='back'></div>
	<div class='honorees'>
		<div class='arrs'>
			<img class='left' src="<?= FRONT_ASSETS ?>img/arr.png">
			<img class='right' src="<?= FRONT_ASSETS ?>img/arr.png">
		</div>
		<div class='honoree'>
			<div class='img' style="background-image: url('<?= FRONT_ASSETS ?>img/otto.jpg')"></div>
			<div class='dark'>
				<p>Otto Cedeno</p>
				<div class='info'>i</div>
			</div>
			<div class='bio'>
				<div class='close'>x</div>
				<p>Otto Cedeno is founder of the highly acclaimed NYC-based taqueria Otto’s Tacos. A former tech startup executive and LA-transplant nostalgic for the authentic Mexican flavors of his Southern California upbringing, Otto opened his flagship East Village shop in 2013, paying an homage to traditional Mexican dishes and the no-frills, trusty, taquerias he would eat at every day before and after school growing up. Filling a void of readily-available, traditional tacos, Otto brings New Yorkers thoughtful but straightforward California-style fare with fresh masa tortillas and signatures like the now famous deep-fried Gorgon.</p>
				<p>Otto's Tacos has been a part of the American Cancer Society community for the past three years, participating in a viral fundraising campaign of making their tortillas "pink" in October to support Breast Cancer Awareness Month.  Proceeds from pink tortilla sales are donated to American Cancer Society's Making Strides Against Breast Cancer in Central Park.</p>
				<p>Prior to his hospitality career, Otto was the Director of Production for Livestream, the seminal live streaming video platform, and is a graduate of the film program at NYU Tisch School of Arts. Each year, Otto speaks at the NYU Entrepreneurs Festival, is a supporter of Venture for America, and an investor in several early-stage startups including Launcher, a Brooklyn-based aerospace company. As a self-proclaimed science nerd, he hopes to someday serve the first taco on Mars. Otto resides in the East Village with his dog Indiana “Indy” Jones.</p> 
			</div>
		</div>
		<div class='honoree'>
			<div class='img' style="background-image: url('<?= FRONT_ASSETS ?>img/mike.jpg')"></div>
			<div class='dark'>
				<p>Michael Lomonaco</p>
				<div class='info'>i</div>
			</div>
			<div class='bio'>
				<div class='close'>x</div>
				<p>Michael Lomonaco is the Chef/Partner of the highly successful Porter House Bar and Grill and cocktail lounge Center Bar, both located in the Time Warner Center, as well as newly opened Hudson Yards Grill in Hudson Yards. A veteran of the New York restaurant scene, Michael has helmed some of NYC’s most iconic kitchens including ‘21’ Club, Windows on the World, Wild Blue, Noche, and Guastavino’s. A well-known face to television audiences, Michael hosted “Epicurious” and “Michael’s Place” on Travel Channel and Food Network, and makes regular appearances on The Today Show and The Chew. He has published two cookbooks, The ‘21’ Cookbook and Nightly Specials.</p>
				<p>Deeply dedicated to community support and charitable causes, Michael co-founded the Windows of Hope Family Relief Fund in the wake of September 11, 2001. He also actively participates in fundraising events that benefit important and worthy causes throughout the city, including City Harvest and The Food Bank of New York.</p>
				<p>In 2016, Porter House Bar and Grill celebrated their 10th anniversary in the Time Warner Center. To mark the occasion, Lomonaco oversaw a cosmetic redesign and relaunch, bringing modern finishes to the timeless space. The following year, the restaurant was awarded “Absolute Best Steakhouse in New York” by New York Magazine, a title they were awarded again in 2018. In March 2019, Michael will open a new concept called Hudson Yards Grill, expanding his culinary footprint to New York’s newest neighborhood.</p> 
			</div>
		</div>
		<div class='honoree'>
			<div class='img' style="background-image: url('<?= FRONT_ASSETS ?>img/ralph.jpg')"></div>
			<div class='dark'>
				<p>Ralph Scamardella</p>
				<div class='info'>i</div>
			</div>
			<div class='bio'>
				<div class='close'>x</div>
				<p>After nearly three decades in the kitchens of the most esteemed restaurants in the world, Ralph Scamardella serves as Partner and Chef of TAO Group, overseeing all chefs and concepts in New York (TAO Uptown, TAO Downtown, LAVO Italian Restaurant, food & beverage at Dream Downtown and Dream Midtown, and food & beverage at Moxy Times Square including Legasea, a seafood brasserie); Las Vegas (TAO Asian Bistro at The Venetian, TAO Beach and LAVO Italian Restaurant at The Palazzo); and Los Angeles (TAO, Luchini, and food & beverage at Dream Los Angeles). As the son of Italian immigrants, Scamardella developed an affinity for cooking with fresh ingredients early on, enjoying seasonal produce as staples in the family’s cooking within their Brooklyn kitchen.</p>
				<p>Scamardella began his studies at New York City’s Technology Institute, learning to master the business of hotel and restaurant management while simultaneously building his culinary skills at The Plaza Hotel’s French restaurant. He then moved on to hone his skills with one of the world’s true master chefs, working under mentor Daniel Boulud at Polo Restaurant. </p>
				<p>Paving the way to distinction early, he received a two-star rating from The New York Times at Vanessa’s restaurant, where he served as Executive Chef. Scamardella went on to have a large hand in the success of the opening and driving concept behind Carmine’s, as chef and partner. After nearly 15 years and several successful projects, he left the kitchen to consult for restaurants such as The Strip House, Harrah’s Casino, and Cessca.</p>
				<p>Upon joining TAO Group in 2007 as Corporate Executive Chef and Partner, Scamardella opened LAVO Italian Restaurant and TAO Beach in Las Vegas. He then went on to open LAVO Italian Restaurant in New York, as well as TAO Downtown in Chelsea, Bodega Negra at Dream Downtown, The Rickey and Fishbowl at Dream Midtown, the Los Angeles locations of TAO and new brand, Luchini as well as Legasea and Magic Hour Rooftop Bar & Lounge at Moxy Times Square. In 2018, Scamardella helped open LAVO Singapore, atop the iconic Marina Bay Sands SkyPark. Most recently, TAO Group’s most recent U.S. expansion introduced TAO Asian Bistro & Nightclub to Chicago, in September 2018.</p>
			</div>
		</div>
		<div class='honoree'>
			<div class='img' style="background-image: url('<?= FRONT_ASSETS ?>img/dana.jpg')"></div>
			<div class='dark'>
				<p>Dana Cowin</p>
				<div class='info'>i</div>
			</div>
			<div class='bio'>
				<div class='close'>x</div>
				<p>Dana Cowin, best known for her two decades as the Editor-in-Chief of Food & Wine Magazine, is a tastemaker, talent scout, consultant, author, lecturer and radio show host. During her tenure at the award-winning magazine (1995-2016), she oversaw every editorial aspect of the brand from print to web to books to social media. After Food & Wine, Cowin joined the ground-breaking restaurant group Chefs Club to continue her role as a scout, selecting chefs from around the world to be featured on their curated menus. Cowin moved on to launch DBC Creative, a pr, branding and culinary connection consultancy with clients such as Dig Inn and Neiman Marcus. Simultaneously, she launched "Speaking Broadly," a podcast, event and content hub focusing on highlighting and linking extraordinary women in the food industry.</p>
				<p>A sought-after speaker, Cowin has been the keynote for Bacardi's Spirit Forward multi-city tour, a judge on Bravo’s “Top Chef,” a presenter at TedXManhattan, a lecturer at the Food & Wine Classic and multiple other food festivals and events.  A noted author, Cowin in 2014 published “Mastering My Mistakes in the Kitchen: Learning to Cook with 65 Great Chefs and Over 100 Delicious Recipes,” which garnered a spot on many “Best of Fall” lists. Cowin serves on the Board of Directors of City Harvest, a hunger-relief organization, and Hot Bread Kitchen, a workforce development group. For her many contributions to the culinary world, Cowin was inducted in to the James Beard Foundation’s Who’s Who of Food & Drink.</p> 
			</div>
		</div>
	</div>
</main>

<script type="text/javascript">
	$(document).ready(function() {
		slide($('.honoree')[0]);
		var timer;

		function slide(s){
			$(s).fadeIn(1000);
			$(s).addClass('show');
			var nxt = $(s).next('.honoree');
			if ( nxt.length < 1 ) {
				nxt = $('.honoree')[0];
			}
			timer = setTimeout(function(){
				$(s).fadeOut(1000);
				$(s).removeClass('show');
				slide(nxt);
			}, 5000);
		}

		$('.honoree .info').click(function(){
			$(this).parents('.honoree').children('.bio').fadeIn(500);
			window.clearTimeout(timer);
		});

		$('.close').click(function(){
			$('.bio').fadeOut(500);
		});

		$('.arrs img').click(function(){
			var nxt = $('.show').next('.honoree');
			if ( $(this).hasClass('left') ) {
				nxt = $('.show').prev('.honoree');
				if ( nxt.length < 1 ) {
					nxt = $('.honoree')[$('.honoree').length-1];
				}
			}else {
				if ( nxt.length < 1 ) {
					nxt = $('.honoree')[0];
				}
			}

			window.clearTimeout(timer);
			$('.show').fadeOut(500);
			$('.show').removeClass('show');
			slide(nxt);
		});
	});
</script>