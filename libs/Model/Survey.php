<?php
namespace Model;

class Survey extends \Emagid\Core\Model {
	static $tablename = "survey";
	public static $fields = [
		'answer',
	];
}